#Imports
import warnings
warnings.filterwarnings('ignore')
from pydocumentdb import document_client
from pandas.io.json import json_normalize
import re
import pandas as pd
import matplotlib.pyplot as plt

import dash
import dash_table
from dash.dependencies import Output, Input
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import os
import flask

dash_app = dash.Dash()
app = dash_app.server

HOST = "https://measurementdata.documents.azure.com:443/"
READ_ONLY_KEY = "OhW7xRQj23uEfs0ucKSz193kNfNoaBnNJlvNofAuU6F6VpVQY8Q1FlVP5uYydwbOF4PPOrv1b1X2TshtlmZsLg=="
DB_ID = "HAWEmeasurements"
COLLECTION_ID = "MeasurementData"
options = {}
options['enableCrossPartitionQuery'] = True
options['maxItemCount'] = 2
client = document_client.DocumentClient(HOST, {'masterKey': READ_ONLY_KEY})
def query_collection(collection, client=client):
    def exec_query(query):
        return iter(client.QueryDocuments(collection["_self"], query, options))
    return exec_query
db = next(db for db in client.ReadDatabases() if db["id"] == DB_ID)
psl_collection = next(coll for coll in client.ReadCollections(db["_self"]) if coll["id"] == COLLECTION_ID)
query_psl = query_collection(psl_collection)

global_df = None
global_full_df = None

def get_documents_by_order_no(order_no):
    '''This function queries the CosmosDB. The provided parameter is the filer value for the field "Auftragsnr.".'''
    appended_items = []
    counter = 0

    query_auftr = """
    SELECT  c.Datum, c["Kommissionsnr."], c["Positionsnr in Batt."], c["Seriennr."], 
            c["Auftragsnr."], c["Seriennr. Ventil"], c.id
    FROM c 
    WHERE c["Auftragsnr."] = """ +order_no 

    for item in query_psl(query_auftr):
        normitem = json_normalize(item)
        appended_items.append(normitem)
        counter += 1
    full_dataframe = pd.concat(appended_items, axis=0)
    full_dataframe = full_dataframe.reset_index()
    full_dataframe = full_dataframe.drop(['index'], axis = 1)
    return(full_dataframe)

def get_document_details(order_no, doc_id):
    '''This function queries the CosmosDB. The provided parameter is the filer value for the field "Auftragsnr.".'''
    appended_items = []
    counter = 0

    query_auftr = """
    SELECT  *
    FROM c 
    WHERE c["Auftragsnr."] = """ +order_no + " and c.id = \"" +doc_id +"\""

    for item in query_psl(query_auftr):
        normitem = json_normalize(item)
        appended_items.append(normitem)
        counter += 1
    full_dataframe = pd.concat(appended_items, axis=0)
    full_dataframe = full_dataframe.reset_index()
    full_dataframe = full_dataframe.drop(['index'], axis = 1)

    print(full_dataframe)

    return(full_dataframe)

    '''Since the function query_db may return mutliple measurements, this function enables the user to select a specific measurement.'''
    Messwerte = data['Messwerte.Values']
    arr = Messwerte.values
    value_dataframe = pd.DataFrame(arr[i])
    return(value_dataframe)

dash_app.layout = html.Div([
    html.Div(className = 'banner', children = [
        html.H1("Cosmos Query"),
        html.Img(src="/static/HAWE_AI_logo2.png"),
        
    ]),
    html.Div(className = "paragraph", children = [
        html.Hr(),
        html.H3("Select a production order no."),
        html.Hr(),
        dcc.Input(id="order_no", value="1039422", type="text"),
        html.Button(id="order_button", n_clicks=0, children="Submit")
    ]),
    html.Div(className = "paragraph", children = [
        html.Hr(),
        html.H3("Select a document"),
        html.Hr(),
        dash_table.DataTable(id="document_table",style_table={'maxHeight':'300','overflowY':'scroll'},row_selectable='single', sorting=True, pagination_mode=False),
        html.Button(id="doc_button", n_clicks=0, children="Select")
    ]),
    html.Div(className = "paragraph", children = [
        html.Hr(),
        html.H3("Documents values:"),
        html.Hr(),
        dcc.Markdown(id="chosen_doc"),
        dash_table.DataTable(id="document_details_table",style_table={'maxHeight':'300','overflowY':'scroll'}, pagination_mode=False)
    ]),
    html.Div(className = "paragraph", children = [
        html.Hr(),
        html.H3("Measurement:"),
        html.Hr(),
        dcc.Graph(id="curve")
    ])
])


#---------------------#
# STATIC FILE LINK
#---------------------#

current_directory = os.getcwd()
allowed_files = ['stylesheet.css', 'HAWE_AI_logo2.png']
static_route = '/static/'

@app.route('{}<file>'.format(static_route))
def serve_file(file):
    if file not in allowed_files:
        raise Exception(
            '"{}" is excluded from the allowed static files'.format(
                file
            )
        )
    return flask.send_from_directory(current_directory, file)


@dash_app.callback([dash.dependencies.Output("document_table", "columns"),dash.dependencies.Output("document_table", "data")],
                [dash.dependencies.Input("order_button", "n_clicks")],
                [dash.dependencies.State("order_no", "value")]
)

def update_document_table(n_clicks, order_no):
    df = get_documents_by_order_no(order_no)
    global global_df
    global_df = df
    df = df.drop(['id'], axis = 1)
    col=[{"name": i, "id": i} for i in df.columns]
    dat=df.to_dict("rows")
    return col, dat


@dash_app.callback([dash.dependencies.Output("chosen_doc", "children"),dash.dependencies.Output("document_details_table", "columns"),dash.dependencies.Output("document_details_table", "data"),dash.dependencies.Output("curve", "figure")],
                [dash.dependencies.Input("doc_button", "n_clicks")],
                [dash.dependencies.State("document_table", "selected_rows"),dash.dependencies.State("order_no", "value")]
)

def update_document_details(n_clicks, row_no, order_no):
    if row_no == None or order_no == None:
        return None
    else:
        global global_df
        print("The doc selection button has been pressed!")
        text = "The selected row is " + global_df.iloc[row_no[0], 6]
        df = get_document_details(order_no, str(global_df.iloc[row_no[0], 6]))
        global global_full_df
        global_full_df = df
        df = df.drop(['Messwerte.Rows'], axis = 1)
        df = df.drop(['Messwerte.Values'], axis = 1)
        df = df.drop(['Messwerte.Columns'], axis = 1)
        col=[{"name": i, "id": i} for i in df.columns]
        dat=df.to_dict("rows")

        series = global_full_df["Messwerte.Values"]
        arr = series.values
        value_dataframe = pd.DataFrame(arr[0])
        
        x=list(value_dataframe[0])
        y=list(value_dataframe[1])
        graph = go.Scatter(x=x,y=y,name="Messkurve",line=dict(color="#e27100"))
        data=[]
        data.append(graph)
        layout = dict(title="Callback Graph", showlegend=False)
        
        fig = dict(data=[graph], layout=layout)

        return text, col, dat, fig

dash_app.css.append_css({
    "external_url": "/static/stylesheet.css"
})


if __name__ == "__main__":
    dash_app.run_server(debug=False)
# Dockerized Dash


This Repo includes a Dash App which has been prepared to be deployed in a docker container.

---

### Why should you put Dash Apps in a Docker Container?

* Scale out for balancing load peaks
* Easy deployment in Azure
* Using Azures builtin Authentication/Authorization Features

---